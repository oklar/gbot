from graviex import API as gx
import pandas as pd
import time
from decimal import Decimal, getcontext, ROUND_DOWN
import operator
from algo import subtractOne, addOne
from datetime import datetime


class Trader:
    class Side:
        BUY = ('buy', 'bids')
        SELL = ('sell', 'asks')

    class Ticker:
        baseCurrency = ""
        quoteCurrency = ""
        market = ""

    class Threshold:
        roi = Decimal("0.025") # The minimum percentage of return of investment to earn in a trade
        order = Decimal("0.0075") # The percentage of next order in orderbook should at least be at
        volume = Decimal("450") # The accumulated volume to either add one or put ask/bid on top of
        rvolume = Decimal("201") # Removes entries with the remaining volume of this amount or lower
        start_sell = Decimal("5") # Start selling if at least have 5 amount of volume in base currency

    class Option:
        fraction = Decimal("0.2") # How much of bankroll to use in a bid or ask.

    def __init__(self, baseCurrency: str, quoteCurrency: str, fraction: str, sleep: int,
                 firstrun: bool=True, side: str='buy', reset: bool=True, my_buy_order_id=None):
        """
        Inititalize the bot
        :param baseCurrency: which coin to buy
        :param quoteCurrency: which coin to buy from
        :param fraction: the fraction of the total bankroll
        :param sleep: how long to sleep between every action
        :param firstrun: (not in use?)
        :param side: (buy, not in use?)
        :param reset: start of by bidding
        :param my_buy_order_id: if there's already bought (should change to get all trades)
        """
        getcontext().rounding = ROUND_DOWN
        self.my_orders = pd.DataFrame()
        self.Ticker.baseCurrency = baseCurrency
        self.Ticker.quoteCurrency = quoteCurrency
        self.Ticker.market = baseCurrency + quoteCurrency
        self.sleep = sleep
        self.side = side
        self.fraction = Decimal(fraction)
        self.first_run = firstrun
        self.reset = reset
        self.my_trades = pd.DataFrame(columns=['id', 'price', 'volume', 'market', 'created_at', 'side'])

        self.account_start = -1
        self.account_begin = -1
        self.account_fraction = -1

        self.my_trade_amount = -1
        self.my_start_bid_volume = -1


        self.my_buy_order = -1

        if my_buy_order_id != None:
            self.my_buy_order_id = my_buy_order_id
            self.reset = False
        else:
            self.my_buy_order_id = -1

        self.my_buy_order_state = -1
        self.my_sell_order = -1
        self.my_sell_order_id = -1
        self.my_sell_order_state = -1

        if reset:
            api.setOrdersClear()
            time.sleep(1)

    def updateDataTrade(self, trades: pd.DataFrame):
        """
        Takes in a dataframe and changes the values such that it applies
        to correct values such as precision.
        :param trades: pandas dataframe
        :return: changed dataframe
        """
        trades['volume'] = trades['volume'].apply(lambda x: self.getPrec(Decimal(x)))
        trades['price'] = trades['price'].apply(lambda x: self.getPrec(Decimal(x)))
        trades['created_at'] = pd.to_datetime(trades['created_at'])
        return trades

    def myTradesPassThresholdVolume(self, trades: pd.DataFrame) -> bool:
        """
        If the sum of the volumes in the trades is over the amount of 5,
        then start selling the base coin.
        TODO:
        Change it up so that if everything is bought, then return true.
        :param trades: pandas dataframe
        :return: bool
        """
        return trades.volume.sum() >= self.Threshold.start_sell

    def updateData(self):
        """
        Updates the dataframes in the beginning of a trade
        :return:
        """
        ob = api.getOrderBook(self.Ticker.market, option={'asks_limit': 40, 'bids_limit': 40})

        self.orderbook_bid  = self.updateDataBook(ob[self.Side.BUY[1]])
        self.orderbook_ask = self.updateDataBook(ob[self.Side.SELL[1]])

        self.account = pd.DataFrame(api.getAccount()['accounts'])
        self.account['balance'] = self.account['balance'].apply(lambda x: Decimal(x))
        self.account_base = self.account[self.account.currency == self.Ticker.baseCurrency]
        self.account_base.reset_index(drop=True, inplace=True)
        self.account_quote = self.account[self.account.currency == self.Ticker.quoteCurrency]
        self.account_quote.reset_index(drop=True, inplace=True)

        if self.reset:
            if self.account_start != -1:
                print(str(datetime.now())
                      + " | Prev balance: " + str(self.account_start)
                      + " | Current balance: " + str(self.account_quote.loc[0, 'balance'])
                      + " | Earned: " + str(self.getPrec(Decimal(self.account_quote.loc[0, 'balance'])) - self.getPrec(Decimal(self.account_start)))
                      + " | Since beginning: " + str(self.account_begin))

            if self.account_begin == -1:
                self.account_begin = self.account_quote.loc[0, 'balance']

            self.account_fraction = self.getPrec(Decimal(self.account_quote.loc[0, 'balance'])) * self.getPrec(Decimal(self.fraction))
            self.account_start = self.account_quote.loc[0, 'balance']

    def updateDataBook(self, api_book):
        """
        Changes the orderbook with relevant information.
        :param api_book: pandas dataframe
        :return: filtered pandas dataframe
        """
        book = pd.DataFrame(api_book)
        book.drop(columns=['at', 'avg_price', 'market', 'ord_type', 'state', 'executed_volume', 'trades_count'], inplace=True)
        book['created_at'] = pd.to_datetime(book['created_at'])
        book['remaining_volume'] = book['remaining_volume'].apply(lambda x: Decimal(x))
        book['price'] = book['price'].apply(lambda x: Decimal(x))
        return book

    def filterData(self):
        """
        Filter out data in the orderbook.
        - Remove duplicates
        - Remove remaining threshold volume
        - Remove next order algo
        :return:
        """
        self.orderbook_bid = self.filterDataBook(self.orderbook_bid)
        self.orderbook_ask = self.filterDataBook(self.orderbook_ask)

    def filterDataBook(self, book):
        """
        Filter the data to get relevant ask and bid price.
        :param book:
        :return:
        """
        fbook = book

        # Checks if there's a buy order, if it is then remove the buy order id
        if self.my_buy_order_id != -1:
            fbook = fbook[fbook.id != self.my_buy_order_id]
            fbook.reset_index(drop=True, inplace=True)

        # Checks if there's a sell order, if it is then remove the sell order id
        if self.my_sell_order_id != -1:
            fbook = fbook[fbook.id != self.my_sell_order_id]
            fbook.reset_index(drop=True, inplace=True)
            # Removes orders where price is not inside a certain threshold
            fbook = fbook[fbook.price >= self.findWeightedTradedThresholdPrice()]
            fbook.reset_index(drop=True, inplace=True)

        # Add duplicate price orders as same orders
        fbook = self.insertAndUpdateDuplicatePriceOrder(book)
        # Remove volumes that is too small to be accounted for
        fbook = fbook[fbook.remaining_volume >= self.Threshold.rvolume]
        fbook.reset_index(drop=True, inplace=True)
        # Does magic
        fbook = self.removeIfNextNotWhitinThreshold(fbook, 'price')
        return fbook

    def findAvgTradedThresholdPrice(self):
        """
        Takes the average of the trades then multiples with a threshold to get
        the relevant price.
        :return: returns price.
        """
        return self.getPrec((self.my_trades.price.sum() / len(self.my_trades)) * (1 + self.Threshold.roi))

    def findWeightedTradedThresholdPrice(self):
        """
        Multiply all the trades with their weight to get the weighted price,
        then multiply by the roi to get the weighted threshold price.
        :return: returns price.
        """
        tot_volume = self.my_trades.volume.sum()
        threshold_price = 0
        for price, volume in zip(self.my_trades.price, self.my_trades.volume):
            threshold_price += price * (volume / tot_volume)

        return self.getPrec(threshold_price * (1 + self.Threshold.roi))

    def insertAndUpdateDuplicatePriceOrder(self, book: pd.DataFrame):
        i = 0
        column = 'price'
        rvolume = 'remaining_volume'

        while i < len(book) - 1:
            if book.loc[i, column] == book.loc[i+1, column]:
                book.loc[i + 1, rvolume] += book.loc[i, rvolume]
                book.drop(index=i, inplace=True)

            i += 1

        book.reset_index(drop=True, inplace=True)
        return book

    def removeIfNextNotWhitinThreshold(self, book: pd.DataFrame, column: str, op=operator.ge):
        """
        If a price is to far off spread in price,
        remove it from the orderbook.
        :param book: pandas dataframe
        :param column: price
        :param op: greater or less depending on bid or ask
        :return: pandas dataframe
        """
        for i in range(len(book[column])):
            j = i + 1
            if j < len(book[column]):
                if op(book[column][i], book[column][j]*(1+self.Threshold.order)):
                    book.drop(index=i, inplace=True)
                else:
                    break
        return book.reset_index(drop=True)

    def findOrderPrice(self, book, side, op=operator.ge):
        if op(book.loc[0, 'remaining_volume'], self.Threshold.volume):
            if side == 'buy':
                return addOne(self.getPrec(book.loc[0, 'price']))
            else:
                return subtractOne(self.getPrec(book.loc[0, 'price']))
        else:
            return self.getPrec(book.loc[0, 'price'])

    def getMyOrders(self):
        self.my_orders = pd.DataFrame(api.getAllOrders(option={'market': self.Ticker.market}))
        return self.my_orders

    def getMyOrdersById(self, id):
        return api.getOrder(id)

    def setOrder(self, side, volume):
        # clear order if on first run
        # set bid
        if side == 'buy':
            #self.my_bid_price = self.getPrec(Decimal(self.my_bid_price / Decimal(1.2)))  # test
            return api.setOrder(self.Ticker.market, side, volume, option={'price': self.my_bid_price})

        if side == 'sell':
            #self.my_ask_price = self.getPrec(Decimal(self.my_bid_price / Decimal(1.2)))  # test
            return api.setOrder(self.Ticker.market, side, volume, option={'price': self.my_ask_price})

    def getPrec(self, value: Decimal, prec: int=9) -> Decimal:
        return Decimal("{:.{}f}".format(value, prec))

    def getTradeVolume(self):
        """
        Volume from ask / sell should be in the amount of what has been sold previously from bid
        In other words, only use this function when buy/selling
        :param side: buy / sell
        :return:
        """
        # d = Decimal('{0:9f}'.format(amount))
        # getcontext().prec = len(d.as_tuple().digits)
        return self.getPrec((self.account_quote.loc[0, 'balance'] * self.fraction) / self.my_bid_price)

    def getTradeVolume2(self):
        trade_volume = 0
        if not self.my_trades.empty:
            trade_volume = self.getPrec(self.my_trades.volume.sum())

        return self.getPrec(self.getPrec(self.account_fraction / self.my_bid_price) - trade_volume)

    def prepareData(self):
        self.my_bid_price = self.findOrderPrice(self.orderbook_bid, 'buy')
        self.my_ask_price = self.findOrderPrice(self.orderbook_ask, 'sell')

    def prepareFirstRun(self):
        if self.reset:
            self.reset = False
            self.my_start_bid_volume = self.getTradeVolume()
            self.my_trade_amount = self.my_start_bid_volume
            self.my_buy_order = self.setOrder('buy', self.my_start_bid_volume)
            self.my_buy_order_id = self.my_buy_order['id']
            self.my_buy_order_state = self.my_buy_order['state']
            print(str(datetime.now())
                  + " | New buy order: " + str(self.my_buy_order['price'])
                  + " | Volume: " + str(self.my_buy_order['volume']))

    def run(self):
        """
        The bots run each time.
        TODO:
        !!!!!!!! Not the most optimal code, there's a lot of redundancy. !!!!!!
        **Known issues:**
        - Sometimes sell on losses, even though it's set up otherwise. Error/update logic failure.
        :return: void
        """

        while True:
            self.updateData()
            self.filterData()
            self.prepareData()
            self.prepareFirstRun()
            self.my_buy_order = self.getMyOrdersById(self.my_buy_order_id)
            self.updateTrades(self.my_buy_order)

            if self.my_buy_order['state'] != 'done':
                if self.checkPriceIsNotEqual('buy', self.my_bid_price, self.my_buy_order['price']):
                    api.setOrdersClear({'side': 'buy'})
                    # self.my_trade_amount = self.getPrec(Decimal(self.my_buy_order['remaining_volume']))
                    self.my_trade_amount = self.getTradeVolume2()
                    self.my_buy_order = self.setOrder('buy', self.my_trade_amount)
                    self.my_buy_order_id = self.my_buy_order['id']
                    print(str(datetime.now())
                          + " | New buy order: " + str(self.my_buy_order['price'])
                          + " | Volume: " + str(self.my_buy_order['volume']))

            # if my trades is not empty then we can start selling
            if not self.my_trades.empty:
                # iff the amount passes a threshold
                if self.myTradesPassThresholdVolume(self.my_trades):
                    # if there is no sell order, set up a new sell order
                    if self.my_sell_order_id == -1:
                        order = self.setOrder('sell', self.getPrec(self.account_base.at[0, 'balance']))
                        # incases where the sell order is outdated
                        if order == False:
                            continue

                        self.my_sell_order = order
                        self.my_sell_order_id = self.my_sell_order['id']
                        print(str(datetime.now())
                              + " | New sell order: " + str(self.my_sell_order['price'])
                              + " | Volume: " + str(self.my_sell_order['volume']))
                    else:
                        self.my_sell_order = self.getMyOrdersById(self.my_sell_order_id)

                        # do not drop trades until buy order is done
                        # this will make sure you don't get a different average
                        # price such that the threshold ROI will be correct
                        if self.my_sell_order['state'] == 'done' and self.my_buy_order['state'] == 'done':
                            self.my_trades.drop(self.my_trades.index, inplace=True)
                            self.my_buy_order = -1
                            self.my_sell_order = -1
                            self.my_sell_order_id = -1
                            self.my_buy_order_id = -1
                            self.reset = True
                            time.sleep(self.sleep)
                            continue

                    # if price found is not equal to previous set price,
                    # change set order.
                    if self.checkPriceIsNotEqual('sell', self.my_ask_price, self.my_sell_order['price']):
                        # cancel sell order place new sell order
                        api.setOrdersClear({'side': 'sell'})
                        self.updateData()
                        self.filterData()
                        self.prepareData()
                        order = self.setOrder('sell',  self.getPrec(self.account_base.at[0, 'balance']))
                        if order == False:
                            continue

                        self.my_sell_order = order
                        self.my_sell_order_id = self.my_sell_order['id']
                        print(str(datetime.now())
                              + " | New sell order: " + str(self.my_sell_order['price'])
                              + " | Volume: " + str(self.my_sell_order['volume']))

            time.sleep(self.sleep)

    def checkPriceIsNotEqual(self, side, p1, p2):
        '''
        Check if price is not equal
        TODO better if statements
        :param side:
        :param p1:
        :param p2:
        :return:
        '''

        if side == 'buy':
            if self.getPrec(Decimal(p1)) != self.getPrec(Decimal(p2)) \
                    and self.getPrec(addOne(self.getPrec(Decimal(p1)))) != self.getPrec(Decimal(p2)):
                return True

        if side == 'sell':
            if self.getPrec(Decimal(p1)) != self.getPrec(Decimal(p2)) \
                    and self.getPrec(subtractOne(self.getPrec(Decimal(p1)))) != self.getPrec(Decimal(p2)):
                return True

        return False

    def updateTrades(self, order):
        pdTradeOrder = pd.DataFrame(order['trades'])
        if not pdTradeOrder.empty:

            if not self.my_trades.empty:
                self.my_trades = self.my_trades.append(pdTradeOrder)
            else:
                self.my_trades = pd.DataFrame(pdTradeOrder)

            self.my_trades = self.updateDataTrade(self.my_trades)
            self.my_trades.drop_duplicates(subset='id', inplace=True)
            self.my_trades.reset_index(drop=True, inplace=True)


if __name__ == '__main__':
    api = gx("key", 'secret')
    bot = Trader('xbi', 'btc', '0.2', 5)
    bot.run()