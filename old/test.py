from enum import Enum

class State(Enum):
    OVERBID = 0
    UNDERASK = 1
    REFACTORBID = 2
    REFACTORASK = 3
    UPDATE = 5
    PAUSE = 6
    DESTROY = 8
    STARTBID = 9
    STARTASK = 10
    PENDING = 11

def overbid():
    print("you overbid")

def switch_demo(argument):
    switcher = {
        State.OVERBID: overbid(),
        2: "February",
        3: "March",
        4: "April",
        5: "May",
        6: "June",
        7: "July",
        8: "August",
        9: "September",
        10: "October",
        11: "November",
        12: "December"
    }
    return switcher.get(argument, overbid())


switch_demo(State.OVERBID)

class Changed:

    def __init__(self):
        self.hallo = "hallo"

class One:

    def __init__(self, api):
        self.api = api

class Two:

    def __init__(self, api):
        self.api = api

changed = Changed()
one = One(changed)
two = Two(changed)

changed.hallo = "hilo"
print(str(changed.hallo))
print("One: " + str(one.api.hallo) + " Two: " + str(two.api.hallo))