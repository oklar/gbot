import sys

from graviex import httpAPI as API
from decimal import Decimal, getcontext
import time
from enum import Enum
import datetime
import handler
import algo
from datetime import datetime, timedelta
import threading
import dateutil.parser
import csv

class State(Enum):
    UPDATE = 0
    OVERBID = 1
    REFACTOR = 3
    START = 2
    DESTROY = 4
    PENDING = 6
    PAUSE = 5


class TRADEBOT:
    baseCurrency = ""
    quoteCurrency = ""
    market = ""
    side = ""
    fractionOfBankroll = 0
    state = State.START
    data = {}
    thresholdRoi = Decimal("8") # threshold in percent
    thresholdVolume = Decimal("0.22")
    thresholdAso = 0.02
    thresholdOverbid = Decimal("1.02")
    thresholdOverask = Decimal("0.98")

    sleepInSeconds = 60
    startroll = 0
    lastroll = 0
    switcher = None

    orderbook = None
    sidebook = 'bids'
    order = None
    orderId = 0
    lastorder = None
    account = None

    asr = 0
    aso = 0
    roi = 0

    overprice = 0
    lastprice = 0
    lastbuyprice = 0
    lastsellprice = 0
    bidVolume = 0
    remainingVolume = 0
    bankroll = 0



    def __init__(self, api):
        self.api = api
        self.switcher = {
            State.UPDATE: self.update,
            State.OVERBID: self.overbid,
            State.REFACTOR: self.refactor,
            State.START: self.start,
            State.DESTROY: self.destroy,
            State.PENDING: self.pending,
            State.PAUSE: self.pause
        }

    def setTradeCurrency(self, baseCurrency, quoteCurrency):
        self.baseCurrency = baseCurrency
        self.quoteCurrency = quoteCurrency
        self.setMarket(baseCurrency + quoteCurrency)

    def setMarket(self, market):
        self.market = market

    def setSide(self, side):
        self.side = side

    def getSide(self):
        return self.side

    def setFractionOfBankroll(self, fractionOfBankroll):
        self.fractionOfBankroll = Decimal(fractionOfBankroll)

    def printConsole(self, string):
        print(str(datetime.now()) + " | " + str(string))

    def updateData(self):
        self.orderbook = api.getOrderBook(bot.market)
        self.account = api.getAccount()
        self.roi = algo.returnOfInvestment(handler.passToLogic(self.orderbook['bids'][0]['price']),
                                           handler.passToLogic(self.orderbook['asks'][0]['price']))

        if self.getSide() == 'buy':
            self.sidebook = 'bids'
        else:
            self.sidebook = 'asks'

        if self.order != None:
            self.order = api.getOrder(self.orderId)
            self.remainingVolume = handler.passToLogic(self.order['remaining_volume'])

    def changeOrder(self):
        if self.getSide() == 'buy':
            self.lastbuyprice = handler.passToLogic(self.order['price'])
            self.setSide('sell')
        else:
            self.lastsellprice = handler.passToLogic(self.order['price'])
            self.setSide('buy')
            time.sleep(0.5)
            self.updateBankroll()

        print(str(self.order))

        self.orderId = 0
        self.order = None
        self.lastprice = 0

    def checkIfAllTradesInOrderIsFinished(self, state):
        if state == 'done':
            return True
        return False

    def checkRoi(self, overprice):
        if self.getSide() == 'buy':
            self.roi = algo.returnOfInvestment(overprice, handler.passToLogic(self.orderbook['asks'][0]['price']))
            if algo.checkThresholdRoi(self.roi, self.thresholdRoi):
                self.printConsole("The ROI: " + '{0:.2f}'.format(float(self.roi)) + " is to small, changing state.")
                self.state = State.PENDING
                return True

        else:
            self.roi = algo.returnOfInvestment(self.lastbuyprice, overprice)
            if algo.checkThresholdRoi(self.roi, self.thresholdRoi):
                self.printConsole("The ROI: " + '{0:.2f}'.format(float(self.roi)) + " is to small, changing state.")
                self.state = State.PENDING
                return True

        return False

    def update(self):
        time.sleep(5)
        self.updateData()

        # self.overprice is the price bidding or asking that should be on our order
        # known bugs:
        #

        # If there's no order check our roi threshold
        # else check roi of set order
        if self.order != None:

            if self.checkIfAllTradesInOrderIsFinished(self.order['state']):
                # Removes the order and change from buy to sell and vise verca
                self.changeOrder()
                return


            if self.checkRoi(algo.theGreatFilter(0, self.thresholdOverbid, self.thresholdOverask, self.getSide(), self.orderbook)):
                return

            # Remove our order from the orderbook
            algo.filterDictFromList(self.orderbook[self.sidebook], {'id': self.orderId})

        else:

            # If there is no order, this means that this is the first time you buy or sell

            self.overprice = algo.theGreatFilter(0, self.thresholdOverbid, self.thresholdOverask, self.getSide(), self.orderbook)

            if self.getSide() == 'buy':
                self.roi = algo.returnOfInvestment(self.overprice, handler.passToLogic(self.orderbook['asks'][0]['price']))
                if algo.checkThresholdRoi(self.roi, self.thresholdRoi):
                    self.printConsole("The ROI: " + '{0:.2f}'.format(float(self.roi)) + " is to small, changing state.")
                    self.state = State.PENDING
                    return

                self.bidVolume = algo.getTradeVolume(self.bankroll, self.fractionOfBankroll, self.overprice)

            else:
                self.roi = algo.returnOfInvestment(self.lastbuyprice, self.overprice)
                if algo.checkThresholdRoi(self.roi, self.thresholdRoi):
                    self.printConsole("The ROI: " + '{0:.2f}'.format(float(self.roi)) + " is to small, changing state.")
                    self.state = State.PENDING
                    return

                self.bidVolume = algo.getBankroll(self.account, self.baseCurrency)

            self.remainingVolume = self.bidVolume

        algo.removeAccumulatedVolumeThreshold(self.thresholdVolume, self.remainingVolume, self.sidebook, self.orderbook)

        # This is the actual overprice to bid/ask
        self.overprice = algo.theGreatFilter(0, self.thresholdOverbid, self.thresholdOverask, self.getSide(), self.orderbook, remove=True)


        # Check if any point in changing price
        # The top of the changed orderbook is equal to our order price. No need to change.
        if self.order != None and algo.isOrderEqualOverprice(self.orderbook, self.order['price'], self.sidebook):
            return

        # If last price you've ordered is not equal to the price you've currently ordered
        # which means you don't need to refactor your price
        if self.lastprice != self.overprice:
            self.state = State.OVERBID

    def overbid(self):
        if self.order == None:
            # calculate the volume based on fraction of bankroll
            if self.getSide() == 'buy':
                self.bidVolume = algo.getTradeVolume(self.bankroll, self.fractionOfBankroll, self.overprice)
            else:
                # else if you're selling calculate the volume based on the basecurrency
                self.bidVolume = handler.passToApiVolume4(algo.getBankroll(self.account, self.baseCurrency))
        else:
            # If there's already an order out there just take the remaining volume
            self.bidVolume = self.order['remaining_volume']

        # check if the price doesn't match eachother ask / bid price
        if self.getSide() == 'buy':
            if self.overprice == handler.passToLogic(self.orderbook['asks'][0]['price']):
                self.state = State.UPDATE
                return
            else:
                time.sleep(1)
                self.api.setOrdersClear({'side': self.getSide()})
        else:
            if self.overprice == handler.passToLogic(self.orderbook['bids'][0]['price']):
                self.state = State.UPDATE
                return
            else:
                time.sleep(1)
                self.api.setOrdersClear({'side': self.getSide()})

        time.sleep(1)
        self.order = self.api.setOrder(self.market, self.side, self.bidVolume, option={'price': handler.passToApi(self.overprice)})
        self.orderId = self.order['id']
        print(str(datetime.now()) +
              " | New " + str(self.getSide()) +
              " | Price: " + str(self.overprice) +
              " | L. Price: " + str(self.lastprice) +
              " | B. Volume: " + str(self.bidVolume) +
              " | R. Volume: " + '{0:.2f}'.format(float(self.remainingVolume)) +
              " | P. ROI: " + '{0:.2f}'.format(float(self.roi)))
        self.lastprice = self.overprice
        self.state = State.UPDATE

    def refactor(self):
        pass

    def start(self):
        pass

    def pending(self):
        time.sleep(2)
        self.printConsole("Pending...")

        if self.order != None:
            if self.order['state'] != 'cancel':
                self.api.setOrdersClear({'side': self.getSide()})
                self.printConsole("Cancelling order.")



        self.state = State.UPDATE

    def pause(self):
        time.sleep(self.sleepInSeconds)
        self.state = State.UPDATE

    # This function needs to be called and NOT switched to
    def destroy(self):
        self.state = State.DESTROY
        api.setOrdersClear()

    def switch(self, argument):
        self.switcher.get(argument, self.destroy)()

    def updateBankroll(self):
        self.bankroll = algo.getBankroll(self.account, self.quoteCurrency)

    def initStartData(self):
        self.updateData()
        self.startroll = algo.getBankroll(self.account, self.quoteCurrency)
        self.bankroll = self.startroll
        self.lastroll = self.startroll

    def run(self):
        # While True, always run after finnishing both buy and a sell
        self.initStartData()
        self.state = State.UPDATE
        while self.state != State.DESTROY:
            self.switch(self.state)



if __name__ == "__main__":
    api = API("", '')
    bot = TRADEBOT(api)
    lastbuy = 0
    try:
        base = sys.argv[1]  # 'xbi'
        quote = sys.argv[2]  # 'btc'
        frac = sys.argv[3]  # 0.2
        side = sys.argv[4]  # 'buy'
    except (ValueError, IndexError):
        base = 'egem'
        quote = 'btc'
        frac = 0.2
        side = 'sell'
    try:
        lastbuy = sys.argv[5]
    except (ValueError, IndexError):
        pass

    if base == 'xbi' and quote == 'btc':
        api.accessKey = ''
        api.secretKey = ''.encode('utf-8')

    if lastbuy != 0:
        bot.lastbuyprice = handler.passToLogic(lastbuy)


    bot.setTradeCurrency(base, quote)
    bot.setFractionOfBankroll(frac)
    bot.setSide(side)

    #api.setOrdersClear()
    bot.run()
    #getAllMarkets = api.getMarkets() # not needed
    #tickers = api.getTickers() # not needed
    #orderBook = api.getOrderBook(bot.market)
    #getTrades = api.getTrades(api.market)
    #getMyTrades = api.getMyTrades(api.market)
    #getOrder = api.getOrder({ 'id': '3087490' })
    #myTrades = api.getMyTrades(api.market)
    #timestamp = api.getTimestamp()
    #deposits = api.getDeposits({ 'currency': 'btc' })
    print("bot ended.")
