from graviex import httpAPI as API
from decimal import Decimal, getcontext
import time
from enum import Enum
import datetime
import handler
import algo

class State(Enum):
    OVERBID = 0
    UNDERASK = 1
    REFACTORBID = 2
    REFACTORASK = 3
    UNWORTHY = 4
    UPDATE = 5
    PAUSE = 6
    DEFAULT = 7
    DESTROY = 8
    STARTBID = 9
    STARTASK = 10
    PENDING = 11


class TRADEBOT:
    baseCurrency = ""
    quoteCurrency = ""
    market = ""
    side = ""
    fractionOfBankroll = 0
    state = State.STARTBID
    data = {}
    # threshold in percent
    thresholdRoi = 5
    sleepInSeconds = 60
    startroll = 0
    lastroll = 0


    def __init__(self, api):
        self.api = api

    def setTradeCurrency(self, baseCurrency, quoteCurrency):
        self.baseCurrency = baseCurrency
        self.quoteCurrency = quoteCurrency
        self.setMarket(baseCurrency + quoteCurrency)

    def setMarket(self, market):
        self.market = market

    def setSide(self, side):
        self.side = side

    def getSide(self):
        return self.side

    def setFractionOfBankroll(self, fractionOfBankroll):
        self.fractionOfBankroll = Decimal(fractionOfBankroll)

    def encodeToDecimal(self, marketData):
        return Decimal('{0:.9f}'.format(float(marketData)))

    def encodeFromDecimalTo9FloatString(self, dec):
        return '{0:.9f}'.format(float(dec))

    def getBankroll(self, accountDataJson, currency):
        for item in accountDataJson['accounts']:
            if item['currency'] == currency:
                return Decimal(item['balance'])
        return False

    def returnOfInvestment(self, topBid, topSell):
        return '{0:.5f}'.format(((float(topSell) / float(topBid)) - 1) * 100)

    def getTradeTopVolume(self, bankroll, fraction, topSide):
        return (bankroll * fraction) / topSide

    def checkOverbid(self, orderPrice, bookTop):
        if (orderPrice < bookTop):
            return True
        return False

    def checkUnderask(self, orderPrice, bookBottom):
        if (orderPrice > bookBottom):
            return True
        return False

    def checkRefactorBid(self, orderPrice, bookPrice):
        if (Decimal(orderPrice) != algo.addOne(Decimal(bookPrice))):
            return True
        return False

    def checkRefactorAsk(self, orderPrice, bookPrice):
        if (Decimal(orderPrice) != algo.subtractOne(Decimal(bookPrice))):
            return True
        return False

    # returns false, thus not needed to complicate the states
    def checkWorth(self, strRoi):
        decroi = Decimal(strRoi)
        if (decroi > self.thresholdRoi):
            return False
        else:
            return True

    def updateDataBid(self, data):
        data['market'] = self.api.getTickersMarket(bot.market)
        data['account'] = self.api.getAccount()
        data['bank'] = self.getBankroll(data['account'], self.quoteCurrency)
        data['topbid'] = self.encodeToDecimal(data['market']['ticker']['buy'])
        data['topask'] = self.encodeToDecimal(data['market']['ticker']['sell'])

        data['bidprice'] = self.encodeFromDecimalTo9FloatString(algo.addOne(data['topbid']))
        data['askprice'] = self.encodeFromDecimalTo9FloatString(algo.subtractOne(data['topask']))
        data['roi'] = self.returnOfInvestment(data['bidprice'], data['askprice'])
        data['bidvolume'] = '{0:.4f}'.format(float(self.getTradeTopVolume(data['bank'], self.fractionOfBankroll, Decimal(data['bidprice']))))

        data['book'] = self.api.getOrderBook(self.market, {'asks_limit': 2, 'bids_limit': 2})
        data['bookBidPrice0'] = self.encodeFromDecimalTo9FloatString(data['book']['bids'][0]['price'])
        data['bookBidPrice1'] = self.encodeFromDecimalTo9FloatString(data['book']['bids'][1]['price'])
        data['bookAskPrice0'] = self.encodeFromDecimalTo9FloatString(data['book']['asks'][0]['price'])
        data['bookAskPrice1'] = self.encodeFromDecimalTo9FloatString(data['book']['asks'][1]['price'])

    def updateDataAsk(self, data):
        data['market'] = self.api.getTickersMarket(bot.market)
        data['account'] = self.api.getAccount()
        data['bank'] = self.getBankroll(data['account'], self.baseCurrency)
        data['topbid'] = self.encodeToDecimal(data['market']['ticker']['buy'])
        data['topask'] = self.encodeToDecimal(data['market']['ticker']['sell'])

        data['bidprice'] = self.encodeFromDecimalTo9FloatString(algo.addOne(data['topbid']))
        data['askprice'] = self.encodeFromDecimalTo9FloatString(algo.subtractOne(data['topask']))
        data['roi'] = self.returnOfInvestment(data['bidprice'], data['askprice'])
        data['bidvolume'] = '{0:.4f}'.format(float(data['bank']))

        data['book'] = self.api.getOrderBook(self.market, {'asks_limit': 2, 'bids_limit': 2})
        data['bookBidPrice0'] = self.encodeFromDecimalTo9FloatString(data['book']['bids'][0]['price'])
        data['bookBidPrice1'] = self.encodeFromDecimalTo9FloatString(data['book']['bids'][1]['price'])
        data['bookAskPrice0'] = self.encodeFromDecimalTo9FloatString(data['book']['asks'][0]['price'])
        data['bookAskPrice1'] = self.encodeFromDecimalTo9FloatString(data['book']['asks'][1]['price'])

    def logPrint(self, type, price):
        print(str(datetime.datetime.now())
              + " " + type + " - "
              + " c.bank: " + str(self.data['bank'])
              + " l.bank: " + str(self.lastroll)
              + " s.bank: " + str(self.startroll)
              + " newbid: " + str(price)
              + " bidvolume: " + str(self.data['bidvolume'])
              + " p.roi: " + str(self.data['roi']))

    def logPrintSale(self, type):
        print(str(datetime.datetime.now())
              + ' ' + type + ': ' + str(self.data['bidprice'])
              + ' p.roi: ' + str(self.data['roi'])
              + ' c.bank: ' + str(self.data['bank'])
              + ' s.bank: ' + str(self.startroll)
              + ' inc. from start: ' + str(self.startroll - self.data['bank']))


    def run(self):
        # While True, always run after finnishing both buy and a sell
        self.data['account'] = self.api.getAccount()
        self.startroll = self.getBankroll(self.data['account'], self.quoteCurrency)
        self.lastroll = self.startroll
        while (True):
            if (self.state == State.UPDATE):
                # checks all the STATES, move to another self.state if needed
                self.data['order'] = self.api.getOrder(self.data['order']['id'])
                if (self.getSide() == 'buy'):
                    self.updateDataBid(self.data)
                    # check if the order is purchased
                    if (self.data['order']['state'] == 'done'):
                        self.setSide('sell')
                        self.logPrintSale('bought')
                        self.state = State.STARTASK
                    elif (self.data['order']['state'] == 'wait'):

                        if (self.checkWorth(self.data['roi'])):
                            print(str(datetime.datetime.now()) + " not worth the risk, state is pending...")
                            self.state = State.PENDING
                        elif (self.checkOverbid(self.data['orderprice'], self.data['bookBidPrice0'])):
                            self.state = State.OVERBID
                        elif (self.checkRefactorBid(self.data['orderprice'], self.data['bookBidPrice1'])):
                            self.state = State.REFACTORBID

                    else:
                        continue

                else:
                    self.updateDataAsk(self.data)
                    if (self.data['order']['state'] == 'done'):
                        self.setSide('buy')
                        self.logPrintSale('sold')
                        self.lastroll = self.getBankroll(self.data['account'], self.quoteCurrency)
                        self.state = State.STARTBID
                    elif (self.data['order']['state'] == 'wait'):

                        if (self.checkUnderask(self.data['orderprice'], self.data['bookAskPrice0'])):
                            self.state = State.UNDERASK
                        elif (self.checkRefactorAsk(self.data['orderprice'], self.data['bookAskPrice1'])):
                            self.state = State.REFACTORASK

                    else:
                        continue


            elif (self.state == State.OVERBID):
                # someone has overbid, you overbid them if possible
                self.api.setOrdersClear({'side': self.getSide()})
                # you cannot bid the same as the actual ask price
                # then you have to bid the same
                if (Decimal(self.data['bidprice']) == self.data['topask']):
                    priceToBid = self.encodeFromDecimalTo9FloatString(self.data['topbid'])
                else:
                    priceToBid = self.encodeFromDecimalTo9FloatString(self.data['bidprice'])

                self.data['bidvolume'] = '{0:.4f}'.format(float(self.getTradeTopVolume(self.lastroll, self.fractionOfBankroll, Decimal(priceToBid))))
                setOrder = self.api.setOrder(self.market, self.side, self.data['bidvolume'],option={'price': priceToBid})
                self.data['order'] = self.api.getOrder(setOrder['id'])
                self.data['orderprice'] = self.encodeFromDecimalTo9FloatString(self.encodeToDecimal(self.data['order']['price']))
                self.logPrint("OVERBID", priceToBid)
                self.state = State.UPDATE

            elif (self.state == State.REFACTORBID):
                # refactor if the second bid least significant digit is not equal my bid by adding one
                self.api.setOrdersClear( {'side': self.getSide()} )
                priceToBid = self.encodeFromDecimalTo9FloatString(algo.addOne(self.encodeToDecimal(self.data['bookBidPrice1'])))
                self.data['bidvolume'] = '{0:.4f}'.format(float(self.getTradeTopVolume(self.lastroll, self.fractionOfBankroll, Decimal(priceToBid))))
                setOrder = self.api.setOrder(self.market, self.side, self.data['bidvolume'], option={'price': priceToBid})
                self.data['order'] = self.api.getOrder(setOrder['id'])
                self.data['orderprice'] = self.encodeFromDecimalTo9FloatString(self.encodeToDecimal(self.data['order']['price']))
                self.logPrint("REFACTORBID", priceToBid)
                self.state = State.UPDATE

            elif (self.state == State.UNDERASK):
                # someone has underask, you undersell them if possible
                self.api.setOrdersClear({'side': self.getSide()})
                # you cannot ask the same as the actual ask price
                # then you have to bid the same
                if (Decimal(self.data['askprice']) == self.data['topbid']):
                    priceToAsk = self.encodeFromDecimalTo9FloatString(self.data['topask'])
                else:
                    priceToAsk = self.encodeFromDecimalTo9FloatString(self.data['askprice'])

                setOrder = self.api.setOrder(self.market, self.side, self.data['bidvolume'],option={'price': priceToAsk})
                self.data['order'] = self.api.getOrder(setOrder['id'])
                self.data['orderprice'] = self.encodeFromDecimalTo9FloatString(self.encodeToDecimal(self.data['order']['price']))
                self.logPrint("UNDERASK", priceToAsk)
                self.state = State.UPDATE

            elif (self.state == State.REFACTORASK):
                # refactor if the second ask least significant digit is not equal my bid by subtracting one
                self.api.setOrdersClear({'side': self.getSide()})
                priceToAsk = self.encodeFromDecimalTo9FloatString(algo.subtractOne(self.encodeToDecimal(self.data['bookAskPrice1'])))
                setOrder = self.api.setOrder(self.market, self.side, self.data['bidvolume'], option={'price': priceToAsk})
                self.data['order'] = self.api.getOrder(setOrder['id'])
                self.data['orderprice'] = self.encodeFromDecimalTo9FloatString(self.encodeToDecimal(self.data['order']['price']))
                self.logPrint("REFACTORASK", priceToAsk)
                self.state = State.UPDATE

            elif (self.state == State.UNWORTHY):
                # if roi is under a certain threshold, escape
                self.state = State.DESTROY
            elif (self.state == State.DESTROY):
                # destroy all orders, make bot stop trading
                self.api.setOrdersClear()
                break
            elif (self.state == State.PAUSE):
                # pause, perhaps for x seconds then make bot start trading again
                time.sleep(self.sleepInSeconds)
                self.state = State.UPDATE
            elif (self.state == State.STARTBID):
                self.updateDataBid(self.data)
                setOrder = self.api.setOrder(self.market, self.side, self.data['bidvolume'], option={'price': self.data['bidprice']})

                self.data['order'] = self.api.getOrder(setOrder['id'])
                self.data['orderprice'] = self.encodeFromDecimalTo9FloatString(self.data['order']['price'])
                self.logPrint("STARTBID", self.data['bidprice'])
                self.state = State.UPDATE
            elif (self.state == State.STARTASK):
                self.updateDataAsk(self.data)
                setOrder = self.api.setOrder(self.market, self.side, self.data['bidvolume'], option={'price': self.data['askprice']})
                self.data['order'] = self.api.getOrder(setOrder['id'])
                self.data['orderprice'] = self.encodeFromDecimalTo9FloatString(self.data['order']['price'])
                self.logPrint("STARTASK", self.data['askprice'])
                self.state = State.UPDATE
            elif (self.state == State.PENDING):
                self.api.setOrdersClear()
                time.sleep(2)
                self.data['market'] = self.api.getTickersMarket(self.market)
                self.data['topbid'] = self.encodeToDecimal(self.data['market']['ticker']['buy'])
                self.data['topask'] = self.encodeToDecimal(self.data['market']['ticker']['sell'])

                self.data['bidprice'] = self.encodeFromDecimalTo9FloatString(algo.addOne(self.data['topbid']))
                self.data['askprice'] = self.encodeFromDecimalTo9FloatString(algo.subtractOne(self.data['topask']))
                self.data['roi'] = self.returnOfInvestment(self.data['bidprice'], self.data['askprice'])
                if (self.checkWorth(self.data['roi'])):
                    self.state = State.PENDING
                else:
                    self.state = State.STARTBID

            elif (self.state == State.DEFAULT):
                # n/a
                self.state = State.UPDATE


if __name__ == "__main__":
    api = API("", '')
    bot = TRADEBOT(api)
    bot.setTradeCurrency('xbi', 'btc')
    bot.setFractionOfBankroll(0.2)
    bot.setSide('buy')

    #api.setOrdersClear()
    #bot.run()
    #getAllMarkets = api.getMarkets() # not needed
    #tickers = api.getTickers() # not needed
    orderBook = api.getOrderBook(bot.market)
    #getTrades = api.getTrades(api.market)
    #getMyTrades = api.getMyTrades(api.market)
    #getOrder = api.getOrder({ 'id': '3087490' })
    #myTrades = api.getMyTrades(api.market)
    #timestamp = api.getTimestamp()
    #deposits = api.getDeposits({ 'currency': 'btc' })
    print("bot ended.")
