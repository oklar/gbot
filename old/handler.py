# This file purpose is to convert data into better data
from decimal import Decimal, getcontext
from algo import subtractOneVolume
import logicv3

# converts string to decimal
def passToLogic(string):
    return Decimal('{0:.9f}'.format(float(string)))

def passToLogic4f(string):
    return Decimal('{0:.4f}'.format(float(string)))

# converts decimal to 9f string
def passToApi(decimal):
    return '{0:.9f}'.format(decimal)

# converts decimal to 4f string
def passToApiVolume4(decimal):
    return subtractOneVolume('{0:.4f}'.format(decimal))

# converts orderbook to use for logic operations
def passOrderBookToLogic(orderbook):
    """Converts the orderbook to manage certain data in logic"""
    return {logicv3.GRAVIEX.Side.BUY[1]: filterOrderbook(orderbook[logicv3.GRAVIEX.Side.BUY[1]]),
            logicv3.GRAVIEX.Side.SELL[1]: filterOrderbook(orderbook[logicv3.GRAVIEX.Side.SELL[1]])}

def filterOrderbook(orderbookside):
    """Filter the orderbook with the appropriate data"""
    return [logicv3.OrderItem(
        item['id'],
        passToLogic(item['price']),
        passToLogic4f(item['remaining_volume']),
        item['created_at'])
        for item in orderbookside]