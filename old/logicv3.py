import sys

from graviex import httpAPI as API
from decimal import Decimal, getcontext
import time
from enum import Enum
import datetime
import handler
import algo
from datetime import datetime, timedelta
import threading
import dateutil.parser
import csv


class GRAVIEX:
    class Side:
        BUY = ('buy', 'bids')
        SELL = ('sell', 'asks')


class OrderItem:
    id = None
    price = None
    rvolume = None
    date = None

    def __init__(self, id, price, rvolume, date):
        self.id = id
        self.price = price
        self.rvolume = rvolume
        self.date = date

    def __repr__(self):
        return str(self.price) + ' | ' + str(self.rvolume) + ' | ' + str(self.date) + ' | ' + str(self.id)

class TRADEBOT:


    class Ticker:
        baseCurrency = ""
        quoteCurrency = ""
        market = ""

    class Threshold:
        roi = Decimal("0.1")
        volume = Decimal("0.22")
        overbid = Decimal("1.02")
        overask = Decimal("0.98")
        over = Decimal("0.02")

    class Option:
        side = GRAVIEX.Side.BUY
        fraction = Decimal("0.2")
        filterLessVolume = Decimal("200")
        filterTime = 45

    class Bank:
        account = None
        roll = None

    class Order:
        book = None
        orders = None
        ids = []

    class Last:
        order = None
        price = None
        id = None
        roll = None

    class Current:
        bid = None
        ask = None
        volume = None

    def __init__(self, api):
        self.api = api

    def setTradeCurrency(self, baseCurrency, quoteCurrency):
        self.Ticker.baseCurrency = baseCurrency
        self.Ticker.quoteCurrency = quoteCurrency
        self.Ticker.market = baseCurrency + quoteCurrency

    def setSide(self, side):
        self.Option.side = side

    def getSide(self):
        return self.Option.side

    def setFractionOfBankroll(self, fraction):
        self.Option.fraction = Decimal(fraction)

    def printConsole(self, string):
        print(str(datetime.now()) + " | " + str(string))

    def updateData(self):
        self.Order.book = handler.passOrderBookToLogic(api.getOrderBook(self.Ticker.market))

        time.sleep(.2)
        self.orderbook = handler.passOrderBookToLogic(api.getOrderBook(self.Ticker.market))
        time.sleep(.2)
        self.Bank.account = api.getAccount()

    def update(self):
        self.updateData()
        side = self.Option.side[0]
        plural_side = self.Option.side[1]
        orderbook_buy = self.Order.book[GRAVIEX.Side.BUY[1]]
        orderbook_sell = self.Order.book[GRAVIEX.Side.SELL[1]]

        orderbook_buy = algo.filterAwayIds(orderbook_buy, self.Order.ids)
        orderbook_buy = algo.filterVolume(orderbook_buy, self.Option.filterLessVolume)
        orderbook_buy = algo.insertAndUpdateDuplicatePriceOrder(orderbook_buy)
        orderbook_buy = algo.removeOldOrders(orderbook_buy, self.Option.filterTime)
        orderbook_buy = algo.removeUntilSatisfiedRoi('bids', 'asks', self.Order.book, self.Threshold.roi)

        orderbook_sell = algo.filterAwayIds(orderbook_sell, self.Order.ids)
        orderbook_sell = algo.filterVolume(orderbook_sell, self.Option.filterLessVolume)
        orderbook_sell = algo.insertAndUpdateDuplicatePriceOrder(orderbook_sell)
        orderbook_sell = algo.removeOldOrders(orderbook_sell, self.Option.filterTime)
        orderbook_sell = algo.removeUntilSatisfiedRoi('asks', 'bids', self.Order.book, self.Threshold.roi)


    def overbid(self):
        self.api.setOrdersClear({'side': self.getSide()})
        self.order = self.api.setOrder(self.market, self.side, self.bidVolume, option={'price': handler.passToApi(self.overprice)})
        self.orderId = self.order['id']
        self.printConsole(
              "New " + str(self.getSide()) +
              " | Price: " + str(self.overprice) +
              " | L. Price: " + str(self.lastprice) +
              " | B. Volume: " + str(self.bidVolume) +
              " | R. Volume: " + '{0:.2f}'.format(float(self.remainingVolume)) +
              " | P. ROI: " + '{0:.2f}'.format(float(self.roi)))
        self.lastprice = self.overprice

    def updateBankroll(self):
        self.bankroll = algo.getBankroll(self.account, self.quoteCurrency)

    def initStartData(self):
        self.updateData()
        self.startroll = algo.getBankroll(self.account, self.quoteCurrency)
        self.bankroll = self.startroll
        self.lastroll = self.startroll

    def run(self):
        while True:
            self.update()


if __name__ == "__main__":
    api = API("", '')
    bot = TRADEBOT(api)
    lastbuy = 0
    try:
        base = sys.argv[1]  # 'xbi'
        quote = sys.argv[2]  # 'btc'
        frac = sys.argv[3]  # 0.2
        side = sys.argv[4]  # 'buy'
    except (ValueError, IndexError):
        base = 'xbi'
        quote = 'btc'
        frac = '0.2'
        side = GRAVIEX.Side.BUY
    try:
        lastbuy = sys.argv[5]
    except (ValueError, IndexError):
        pass

    if base == 'xbi' and quote == 'btc':
        api.accessKey = ''
        api.secretKey = ''.encode('utf-8')

    if lastbuy != 0:
        bot.lastbuyprice = handler.passToLogic(lastbuy)

    bot.setTradeCurrency(base, quote)
    bot.setFractionOfBankroll(frac)
    bot.setSide(side)

    #api.setOrdersClear()
    bot.run()
    #getAllMarkets = api.getMarkets() # not needed
    #tickers = api.getTickers() # not needed
    #orderBook = api.getOrderBook(bot.market)
    #getTrades = api.getTrades(api.market)
    #getMyTrades = api.getMyTrades(api.market)
    #getOrder = api.getOrder({ 'id': '3087490' })
    #myTrades = api.getMyTrades(api.market)
    #timestamp = api.getTimestamp()
    #deposits = api.getDeposits({ 'currency': 'btc' })
    print("bot ended.")
