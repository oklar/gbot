# This file purpose is to help the logic trading bot to do more complicated tasks
from decimal import Decimal, getcontext
import handler
from datetime import datetime, timedelta
import dateutil.parser
import logicv3

# Removes all occurrences of a key in a dict from a list of dicts with specifications
# eg option = { 'id' : 12345678 }
def filterDictFromList(lst, option):
    for k in option:
        lst[:] = [d for d in lst if d.get(k) != option[k]]

# Check if the order id is equal to other id
def validateOrder(orderId, otherOrderId):
    if (orderId == otherOrderId):
        return True
    else:
        return False

def isOrderEqualOverprice(orderbook, orderprice, sidebook):
    if handler.passToLogic(orderbook[sidebook][0]['price']) == handler.passToLogic(orderprice):
        return True
    return False

# Removes all entries which doesn't pass the volume threshold i.e small order volumes
def removeAccumulatedVolumeThreshold(threshold, myvolume, sidebook, orderbook):
    accVolume = 0
    i = 0

    while (i < len(orderbook[sidebook])):
        item = orderbook[sidebook][i]
        accVolume = handler.passToLogic(item['volume']) + accVolume
        if accVolume > myvolume * threshold:
            break
        else:
            del orderbook[sidebook][i]

# The great filter will attempt to remove high spread orders, unless they pass the test of time
# and will place itself where it sees fit.
def theGreatFilter(attempts, thresholdBid, thresholdAsk, side, orderbook, remove=False):
    if side == 'buy':
        obbp0 = handler.passToLogic(orderbook['bids'][0]['price'])
        obbp1 = handler.passToLogic(orderbook['bids'][1]['price'])
        obbt0 = dateutil.parser.parse(orderbook['bids'][0]['created_at'])
        smobbt0 = datetime.now(tz=obbt0.tzinfo) - timedelta(minutes=25)


        # if the bid passes the test of time
        if obbt0 < smobbt0:
            return addOne(obbp0)
        else:
            # check the first bad order spread from the second bid
            # and compare it with the first order
            # if within threshold
            if obbp1 * thresholdBid > obbp0:
                return addOne(obbp0)
            else:
                # if more than one order is having bad order spread
                if attempts >= 1:
                    return addOne(obbp1)
                else:
                    if remove:
                        del orderbook['bids'][0]
                        del orderbook['bids'][1]
                    attempts = attempts + 1
                    return theGreatFilter(attempts, thresholdBid, thresholdAsk, side, orderbook, remove)
    else:
        obap0 = handler.passToLogic(orderbook['asks'][0]['price'])
        obap1 = handler.passToLogic(orderbook['asks'][1]['price'])
        obat0 = dateutil.parser.parse(orderbook['bids'][0]['created_at'])
        smobat0 = datetime.now(tz=obat0.tzinfo) - timedelta(minutes=25)

        if obat0 < smobat0:
            return subtractOne(obap0)
        else:
            if obap1 * thresholdAsk < obap0:
                return subtractOne(obap0)
            else:
                if attempts >= 1:
                    return subtractOne(obap0)
                else:
                    if remove:
                        del orderbook['asks'][0]
                        del orderbook['asks'][1]
                    attempts = attempts + 1
                    return theGreatFilter(attempts, thresholdBid, thresholdAsk, side, orderbook, remove)

# The great filter will attempt to remove high spread orders, unless they pass the test of time
# and will place itself where it sees fit.
def theGreatFilterV2(attempts, thresholdBid, thresholdAsk, ordersidebook, remove=False):
    obbp0 = handler.passToLogic(ordersidebook[0]['price'])
    obbp1 = handler.passToLogic(ordersidebook[1]['price'])
    obbt0 = dateutil.parser.parse(ordersidebook[0]['created_at'])
    smobbt0 = datetime.now(tz=obbt0.tzinfo) - timedelta(minutes=25)

    # if bid is outside roi:

    # if the bid passes the test of time
    if obbt0 < smobbt0:
        return addOne(obbp0)
    else:
        # check the first bad order spread from the second bid
        # and compare it with the first order
        # if within threshold
        if obbp1 * thresholdBid > obbp0:
            return addOne(obbp0)
        else:
            # if more than one order is having bad order spread
            if attempts >= 1:
                return addOne(obbp1)
            else:
                if remove:
                    del ordersidebook[0]
                    del ordersidebook[1]
                attempts = attempts + 1
                return theGreatFilter(attempts, thresholdBid, thresholdAsk, ordersidebook, remove)

def getThresholdFilterSide(side, threshold):
    if side == logicv3.GRAVIEX.Side.BUY[0]:
        return Decimal("1") + threshold
    else:
        return Decimal("1") - threshold


def removeUntilSatisfiedRoi(side, otherside, orderbook, roi):
    i = 0
    while i < len(orderbook[side]) - 1:
        if roi > returnOfInvestment(orderbook[side][0].price, orderbook[otherside][0].price):
            del orderbook[side][i]
        else:
            return orderbook[side]



def theGreatFilterV3(orderbookside, side, minutes, seekLimit, threshold=Decimal("0.02")):
    """Remove orders that is new and not whitin threshold"""
    for i in range(len(orderbookside)):
        dt_order1_date = dateutil.parser.parse(orderbookside[i].date)
        dt_now_minus_time = datetime.now(tz=dt_order1_date.tzinfo) - timedelta(minutes=minutes)

        if dt_order1_date > dt_now_minus_time:
            """if order1 is bigger than (dt now subtracted by minutes),
            which means dt_order1 is whitin time threshold iow: dt_order_1_date is a new bid/ask.
            This means that the two next order should be within threshold of order1"""
            removeThresholdPriceFromOrderbook(i, orderbookside, seekLimit, side, threshold)
        else:
            removeThresholdPriceFromOrderbook(i, orderbookside, seekLimit, side, threshold*2)

    return orderbookside


def removeThresholdPriceFromOrderbook(i, orderbookside, seekLimit, side, threshold):
    seek = 0
    for j in range(i + 1, len(orderbookside)):
        if side == logicv3.GRAVIEX.Side.BUY[0]:
            if orderbookside[i].price > orderbookside[j].price * getThresholdFilterSide(side, threshold):
                """If the order price is still bigger than the next two order, remove the order"""
                del orderbookside[i]
                break
        else:
            if orderbookside[i].price < orderbookside[j].price * getThresholdFilterSide(side, threshold):
                """If the order price is still less than the next two order, remove the order"""
                del orderbookside[i]
                break

        if seek < seekLimit:
            break

        seek += 1


# Average Spread Order (ASO), return the average spread within x(default) days
# Should not take into account my own order, or orders where spread are higher than roi
# algo.averageSpreadOrder(self.orderbook['bids'])
def averageSpreadOrder(orderbookside, myOrderId=None, daysToIgnore=2):
    avglst = []
    # Find the timezone info and place it inside subtraction days
    dtparse = dateutil.parser.parse(orderbookside[0]['created_at'])
    subtractDays = datetime.now(tz=dtparse.tzinfo) - timedelta(days=daysToIgnore)
    lenbook = len(orderbookside)
    i = 0 # Don't count the overbid

    while i+1 < lenbook:
        ob0 = orderbookside[i]
        ob1 = orderbookside[i+1]

        dtparse0 = dateutil.parser.parse(ob0['created_at'])
        dtparse1 = dateutil.parser.parse(ob1['created_at'])

        # Do not count orders which are more than x days old
        if dtparse0 < subtractDays or dtparse1 < subtractDays:
            i = i + 1
            continue

        op0 = handler.passToLogic(ob0['price'])
        op1 = handler.passToLogic(ob1['price'])
        avg = op1 / op0

        # Exclude 1
        if avg != 1:
            avglst.append(avg)

        i = i + 1

    sum = 0
    for avg in avglst:
        sum = sum + avg

    return sum / len(avglst)

# Average Spread Roi (ASR), return the average spread within x(default) days
# Should not take into account my own order, or orders where spread are higher than roi
# algo.averageSpreadRoi(self.orderbook['bids'], self.orderbook['asks'][0]['price'])
def averageSpreadRoi(orderbookside, askOrderPrice, myOrderId=None, daysToIgnore=2):
    avglst = []
    # Find the timezone info and place it inside subtraction days
    dtparse = dateutil.parser.parse(orderbookside[0]['created_at'])
    subtractDays = datetime.now(tz=dtparse.tzinfo) - timedelta(days=daysToIgnore)
    lenbook = len(orderbookside)
    i = 0

    askp = handler.passToLogic(askOrderPrice)
    while i < lenbook:
        ob0 = orderbookside[i]

        dtparse0 = dateutil.parser.parse(ob0['created_at'])

        # Do not count orders which are more than x days old
        if dtparse0 < subtractDays:
            i = i + 1
            continue

        op0 = handler.passToLogic(ob0['price'])
        avg = (1 - (op0 / askp)) * 100
        avglst.append(avg)
        i = i + 1

    sum = 0
    for avg in avglst:
        sum = sum + avg

    return sum / len(avglst)

# Add one at the end of decimal
def addOne(decimal):
    _d = Decimal('{0:9f}'.format(decimal))
    getcontext().prec = len(_d.as_tuple().digits)
    return _d.next_plus()

# Subtract one at the end of decimal
def subtractOne(decimal):
    d = Decimal('{0:9f}'.format(decimal))
    getcontext().prec = len(d.as_tuple().digits)
    return d.next_minus()

# Subtract one at the end of decimal
def subtractOneVolume(string):
    d = Decimal(string)
    getcontext().prec = len(d.as_tuple().digits)
    return d.next_minus()

def getBankroll(accountData, currency):
    for item in accountData['accounts']:
        if item['currency'] == currency:
            return handler.passToLogic(item['balance'])
    return False

def returnOfInvestment(topBid, topSell):
    return ((topSell / topBid) - 1) * 100

def getTradeVolume(bankroll, fraction, price):
    return (bankroll * fraction) / price

def checkOverbid(orderPrice, bookTop):
    if orderPrice < bookTop:
        return True
    return False

def checkUnderask(orderPrice, bookBottom):
    if orderPrice > bookBottom:
        return True
    return False

def checkRefactorBid(orderPrice, bookPrice):
    if orderPrice != addOne(bookPrice):
        return True
    return False

def checkRefactorAsk(orderPrice, bookPrice):
    if orderPrice != subtractOne(bookPrice):
        return True
    return False

def checkThresholdRoi(roi, thresholdRoi):
    if (roi < thresholdRoi):
        return True
    else:
        return False

def filterVolume(orderbookside, filterVolume):
    return [b for b in orderbookside if b.rvolume > filterVolume]

def filterAwayIds(orderbookside, ids):
    return [b for b in orderbookside if b.id not in ids]

def insertAndUpdateDuplicatePriceOrder(orderbookside):
    book = orderbookside
    i = 0
    while i < len(book) - 1:
        if book[i].price == book[i+1].price:
            book[i + 1].rvolume += book[i].rvolume
            del book[i]
        else:
            i += 1
            
    return book


def removeOldOrders(orderbookside, time):
    """Remove orders that is new and not whitin threshold"""
    book = orderbookside
    i = 0
    while i < len(book):
        dt_order1_date = dateutil.parser.parse(book[i].date)
        dt_now_minus_time = datetime.now(tz=dt_order1_date.tzinfo) - timedelta(minutes=time)

        if dt_order1_date < dt_now_minus_time:
            """if order1 is less than (dt now subtracted by minutes),
            which means dt_order1 is not whitin time threshold iow: 
            dt_order_1_date is an old bid/ask."""
            del book[i]
        else:
            i += 1

    return book