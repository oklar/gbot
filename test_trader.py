import unittest
from unittest import TestCase

from tbot import Trader
from decimal import Decimal


class TestTrader(TestCase):
    def test_findWeightedTradedThresholdPrice_isCorrect(self):
        bot = Trader('xbi', 'btc', '0.2', 5, reset=False)
        bot.Threshold.roi = Decimal("0.025")
        order = {}
        order['trades'] = [
            {"id": 2, "price": "0.000030200", "volume": "385.8084", "market": "xbibtc",
             "created_at": "2014-04-18T02:04:49Z", "side": "sell"},
            {"id": 3, "price": "0.000030600", "volume": "125.0000", "market": "xbibtc",
             "created_at": "2014-04-18T02:04:49Z", "side": "sell"}
        ]

        bot.updateTrades(order)
        wptroi = bot.findWeightedTradedThresholdPrice()
        self.assertEqual(wptroi, bot.getPrec(0.000031055))

    def test_fail(self):
        bot = Trader('xbi', 'btc', '0.2', 5, reset=False)
        bot.updateData()
        bot.filterData()
        bot.prepareData()
