# This file purpose is to help the logic trading bot to do more complicated tasks
from decimal import Decimal, getcontext


# Add one at the end of decimal
def addOne(decimal):
    _d = Decimal('{0:9f}'.format(decimal))
    getcontext().prec = len(_d.as_tuple().digits)
    return _d.next_plus()

# Subtract one at the end of decimal
def subtractOne(decimal):
    d = Decimal('{0:9f}'.format(decimal))
    getcontext().prec = len(d.as_tuple().digits)
    return d.next_minus()
