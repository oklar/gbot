import hashlib
import hmac
#import urllib.request
import requests
import time
import ssl
from decimal import Decimal, getcontext
import logging
import urllib.request
import urllib.parse
import urllib
import urllib3
import datetime
from datetime import datetime

class API:
    accessKey = ""
    secretKey = ""
    timeLimit = 1
    timeLimitSetRequest = 0.1
    apiGraviexVersion = '/v2/'
    apiCallList = {
        "markets": apiGraviexVersion + "markets",
        "tickers": apiGraviexVersion + "tickers",
        "tickersMarket": apiGraviexVersion + "tickers/",
        "orderBook": apiGraviexVersion + "order_book",
        "depth": apiGraviexVersion + "depth",
        "trades": apiGraviexVersion + "trades",
        "k": apiGraviexVersion + "k",
        "kWithPendingTrades": apiGraviexVersion + "k_width_pending_trades",
        "timestamp": apiGraviexVersion + "timestamp"
    }
    apiCallPrivateList = {
        "members": apiGraviexVersion + "members/me",
        "deposits": apiGraviexVersion + "deposits",
        "deposit": apiGraviexVersion + "deposit",
        "depositAddress": apiGraviexVersion + "deposit_address",
        "orders": apiGraviexVersion + "orders",
        "ordersMulti": apiGraviexVersion + "orders/multi",
        "ordersClear": apiGraviexVersion + "orders/clear",
        "order": apiGraviexVersion + "order",
        "orderDelete": apiGraviexVersion + "order/delete",
        "tradesMy": apiGraviexVersion + "trades/my"
    }

    def __init__(self, accessKey, secretKey):
        self.setKeys(accessKey, secretKey)

    def setKeys(self, accessKey, secretKey):
        self.accessKey = accessKey
        self.secretKey = secretKey.encode('utf-8')

    def setTimeLimit(self, timeLimit):
        self.timeLimit = timeLimit

    def setTimeLimitRequest(self, timeLimit):
        self.timeLimitSetRequest = timeLimit

    def getPayloadString(self, load):
        return 'https://graviex.net/api' + load

    def getEncodedMessage(self, method, load, request):
        return str.encode(method + '|/api' + load + '|' + request)

    def encodeUrl(self, dict):
        return urllib3.request.urlencode(dict)

    def getEpochTime(self, addHours):
        return str(int((datetime.datetime.fromtimestamp(time.time()) + datetime.timedelta(hours=addHours)).timestamp()))


    def deleteOptionDict(self, dict, option={}):
        for item in option:
            del dict[item]
        return dict

    def prepareDict(self, option={}):
        '''
        Easy prepare a dictionary before setting a request.
        :param option:
        :return:
        '''
        opt = {}
        opt['access_key'] = self.accessKey
        opt['tonce'] = str(self.getTimestamp()) + '000'
        opt.update(option)
        return opt

    def genNewDataPayload(self, httpMethod, apiCall, data):
        '''
        Generates new payload with changed tonce and signature with the same data.
        :param httpMethod:
        :param apiCall:
        :param data:
        :return:
        '''
        data['tonce'] = str(self.getTimestamp()) + '000'
        if 'signature' in data:
            del data['signature']
            msg = self.getEncodedMessage(httpMethod, apiCall, self.encodeUrl(data))
            data['signature'] = str(hmac.new(self.secretKey, msg, hashlib.sha256).hexdigest())

        return data

    def setRequest(self, httpMethod, apiCall, timeLimit, data={}, attempts=1):
        '''
        Sets a request towards server.
        :param httpMethod:
        :param apiCall:
        :param timeLimit:
        :param data:
        :param attempts:
        :return:
        '''
        attempts = attempts + 1
        if attempts > 10:
            return False

        try:
            if (httpMethod == 'GET' or httpMethod == 'get'):
                r = requests.get(self.getPayloadString(apiCall), verify=True, data=data)
            else:
                r = requests.post(self.getPayloadString(apiCall), verify=True, data=data)

            r.raise_for_status()
        except requests.exceptions.HTTPError as e:
            print(str(datetime.now()) + " | " + str(attempts) + " | " + str(e) + str(apiCall) + " | " + str(data))
            time.sleep(timeLimit)
            time.sleep(int(3 * attempts))
            r = self.setRequest(httpMethod, apiCall, timeLimit, self.genNewDataPayload(httpMethod, apiCall, data), attempts=attempts)
        except requests.exceptions.RequestException as e:
            print(str(datetime.now()) + " | " + str(attempts) + " | " + str(e) + str(apiCall) + " | " + str(data))
            time.sleep(timeLimit)
            time.sleep(int(3 * attempts))
            r = self.setRequest(httpMethod, apiCall, timeLimit, self.genNewDataPayload(httpMethod, apiCall, data), attempts=attempts)
        return r


    ################
    # GRAVIEX API: #
    ################

    def getMarkets(self):
        '''
        Get all available markets.
        :return:
        '''
        return self.setRequest('GET', self.apiCallList['markets'], self.timeLimitSetRequest).json()

    def getTickers(self):
        '''
        Get ticker of all markets.
        :return:
        '''
        return self.setRequest('GET', self.apiCallList['tickers'], self.timeLimitSetRequest).json()

    def getTickersMarket(self, market):
        '''
        Get ticker of specific market.
        :param market:
        :return:
        '''
        return self.setRequest('GET', self.apiCallList['tickersMarket'] + market, self.timeLimitSetRequest).json()

    def getAccount(self, option={}):
        '''
        Get your profile and accounts info.
        :param option:
        :return:
        '''
        time.sleep(self.timeLimit)
        opt = self.prepareDict(option=option)
        msg = self.getEncodedMessage('GET', self.apiCallPrivateList['members'], self.encodeUrl(opt))
        opt['signature'] = hmac.new(self.secretKey, msg, hashlib.sha256).hexdigest()
        return self.setRequest('GET', self.apiCallPrivateList['members'], self.timeLimitSetRequest, data=opt).json()

    def getDeposits(self, option={}):
        '''
        Get your deposits history. Dict e.g { currency, limit, state }
        :param option:
        :return:
        '''
        time.sleep(self.timeLimit)
        opt = self.prepareDict(option=option)
        msg = self.getEncodedMessage('GET', self.apiCallPrivateList['deposits'], self.encodeUrl(opt))
        opt['signature'] = hmac.new(self.secretKey, msg, hashlib.sha256).hexdigest()
        return self.setRequest('GET', self.apiCallPrivateList['deposits'], self.timeLimitSetRequest, data=opt).json()

    def getDeposit(self, txid):
        '''
        Get details of specific deposit.
        :param txid:
        :return:
        '''
        time.sleep(self.timeLimit)
        opt = self.prepareDict(option={ 'txid': txid })
        msg = self.getEncodedMessage('GET', self.apiCallPrivateList['deposit'], self.encodeUrl(opt))
        opt['signature'] = hmac.new(self.secretKey, msg, hashlib.sha256).hexdigest()
        return self.setRequest('GET', self.apiCallPrivateList['deposit'], self.timeLimitSetRequest, data=opt).json()

    def getDepositAddress(self, currency):
        '''
        Where to deposit. The address field could be empty when a new address is generating (e.g. for bitcoin), you should try again later in that case.
        :param currency:
        :return:
        '''
        time.sleep(self.timeLimit)
        opt = self.prepareDict(option={'currency': currency})
        msg = self.getEncodedMessage('GET', self.apiCallPrivateList['depositAddress'], self.encodeUrl(opt))
        opt['signature'] = hmac.new(self.secretKey, msg, hashlib.sha256).hexdigest()
        return self.setRequest('GET', self.apiCallPrivateList['depositAddress'], self.timeLimitSetRequest, data=opt).json()

    def getAllOrders(self, option={}):
        '''
        Get your orders, results is paginated.
        :param option:
        :return:
        '''
        time.sleep(self.timeLimit)
        # opt = self.prepareDict(option=option)
        opt = {'access_key': self.accessKey}
        opt.update(option)
        opt['tonce'] = str(self.getTimestamp()) + '000'
        msg = self.getEncodedMessage('GET', self.apiCallPrivateList['orders'], self.encodeUrl(opt))
        opt['signature'] = str(hmac.new(self.secretKey, msg, hashlib.sha256).hexdigest())
        return self.setRequest('GET', self.apiCallPrivateList['orders'], self.timeLimitSetRequest, data=opt).json()

    def setOrder(self, market, side, volume, option={}):
        '''
        Create a Sell/Buy order. Dict e.g { price, ord_type }
        :param market:
        :param side:
        :param volume:
        :param option:
        :return:
        '''
        time.sleep(self.timeLimit)
        opt = {'access_key': self.accessKey, 'market': market }
        opt['price'] = option['price']
        opt.update({'side': side, 'tonce': str(self.getTimestamp()) + '000', 'volume': volume})
        msg = self.getEncodedMessage('POST', self.apiCallPrivateList['orders'], self.encodeUrl(opt))
        opt['signature'] = str(hmac.new(self.secretKey, msg, hashlib.sha256).hexdigest())
        r = self.setRequest('POST', self.apiCallPrivateList['orders'], self.timeLimitSetRequest, data=opt)

        if r != False:
            return r.json()
        else:
            return r

    def setOrdersClear(self, option: dict = {}) -> object:
        '''
        Cancel all my orders. Dict e.g { side }
        :param option:
        :return:
        '''
        time.sleep(self.timeLimit)
        opt = {'access_key': self.accessKey}
        opt.update(option)
        #opt.update({'tonce': str(self.getTimestamp() + '000'})
        opt.update({'tonce': str(self.getTimestamp()) + '000'})
        #opt = self.prepareDict(option=option)
        msg = self.getEncodedMessage('POST', self.apiCallPrivateList['ordersClear'], self.encodeUrl(opt))
        opt['signature'] = str(hmac.new(self.secretKey, msg, hashlib.sha256).hexdigest())
        return self.setRequest('POST', self.apiCallPrivateList['ordersClear'], self.timeLimitSetRequest, data=opt).json()

    def getOrder(self, id):
        '''
        Get information about a specific order.
        :param id:
        :return:
        '''
        time.sleep(self.timeLimit)
        opt = {}
        opt['access_key'] = self.accessKey
        opt['id'] = str(id)
        opt['tonce'] = str(self.getTimestamp()) + '000'
        msg = self.getEncodedMessage('GET', self.apiCallPrivateList['order'], self.encodeUrl(opt))
        opt['signature'] = str(hmac.new(self.secretKey, msg, hashlib.sha256).hexdigest())
        return self.setRequest('GET', self.apiCallPrivateList['order'], self.timeLimitSetRequest, data=opt).json()

    def setOrderDelete(self, id):
        '''
        Cancel an order.
        :param id:
        :return:
        '''
        time.sleep(self.timeLimit)
        opt = {'access_key': self.accessKey, 'tonce': str(self.getTimestamp()) + '000', 'id': id }
        #opt = self.prepareDict(option={ 'id': id })
        msg = self.getEncodedMessage('POST', self.apiCallPrivateList['orderDelete'], self.encodeUrl(opt))
        opt['signature'] = str(hmac.new(self.secretKey, msg, hashlib.sha256).hexdigest())

        opt1 = {'access_key': opt['access_key'], 'id': opt['id'], 'tonce': opt['tonce'], 'signature': opt['signature'] }
        return self.setRequest('POST', self.apiCallPrivateList['orderDelete'], self.timeLimitSetRequest, data=opt1).json()

    def getOrderBook(self, market, option={}):
        '''
        Get the order book of specified market. Dict e.g { asks_limit, bids_limit }
        :param market:
        :param option:
        :return:
        '''
        opt = {}
        opt['market'] = market
        opt.update(option)
        return self.setRequest('GET', self.apiCallList['orderBook'], self.timeLimitSetRequest, data=opt).json()

    def getDepth(self, market, option={}):
        '''
        Get depth or specified market. Both asks and bids are sorted from highest price to lowest. Dict e.g { limit }
        :param market:
        :param option:
        :return:
        '''
        opt = {}
        opt['market'] = market
        opt.update(option)
        return self.setRequest('GET', self.apiCallList['depth'], self.timeLimitSetRequest, data=opt).json()

    def getTrades(self, market, option={}):
        '''
        Get recent trades on market, each trade is included only once. Trades are sorted in reverse creation order. Dict e.g { limit, timestamp, from, to, order_by }
        :param market:
        :param option:
        :return:
        '''
        opt = {}
        opt['market'] = market
        opt.update(option)
        return self.setRequest('GET', self.apiCallList['trades'], self.timeLimitSetRequest, data=opt).json()

    def getMyTrades(self, market, option={}):
        '''
        Get your executed trades. Trades are sorted in reverse creation order. Dict e.g { limit, timestamp, from, to, order_by }
        :param market:
        :param option:
        :return:
        '''
        time.sleep(self.timeLimit)
        opt = self.prepareDict(option=option)
        opt['market'] = market
        msg = self.getEncodedMessage('GET', self.apiCallPrivateList['tradesMy'], self.encodeUrl(opt))
        opt['signature'] = hmac.new(self.secretKey, msg, hashlib.sha256).hexdigest()
        return self.setRequest('GET', self.apiCallPrivateList['tradesMy'], self.timeLimitSetRequest, data=opt).json()

    def getK(self, market, option={}):
        '''
        Get OHLC(k line) of specific market. Dict e.g { limit, period, timestamp }
        :param market:
        :param option:
        :return:
        '''
        opt = {}
        opt['market'] = market
        opt.update(option)
        return self.setRequest('GET', self.apiCallList['k'], self.timeLimitSetRequest, data=opt).json()

    def getKWithPendingTrades(self, market, trade_id, option={}):
        '''
        Get K data with pending trades, which are the trades not included in K data yet, because there's delay between trade generated and processed by K data generator. Dict e.g { limit, period, timestamp }
        :param market:
        :param trade_id:
        :param option:
        :return:
        '''
        opt = {}
        opt['market'] = market
        opt['trade_id'] = trade_id
        opt.update(option)
        return self.setRequest('GET', self.apiCallList['kWithPendingTrades'], self.timeLimitSetRequest, data=opt).json()

    def getTimestamp(self):
        '''
        Get timestamp.
        :return:
        '''
        return self.setRequest('GET', self.apiCallList['timestamp'], self.timeLimitSetRequest).json()

