**This is for _experiment only_ and should not be used whatsoever.**

The code was written pretty fast and it is not optimized, there's also code redundancy.

Feel free to use all or some of the code.

The API is straight forward, although some functionality is not working due to backend problems.


**DISCLAIMER:**
- The author claims zero responsibility on anything (that might happen), 
such as losses of currency, during the use of this program or any algorithm written.

